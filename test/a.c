/* Test program runner for ion.
   Copyright (C) 2013-2024 Sergey Poznyakoff

   Ion is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Ion is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with ion. If not, see <http://www.gnu.org/licenses/>. */

/* The program runs the command supplied in its command line, waits for
   the command line prompt to appear on its stdout and feeds a word to
   its stdin once it does.  Then it reads the response from it and checks
   if it consists the same characters as the input word, but in reversed
   order.  The action is repeated for several words in sequence.

   Exit code is 0 (success) if all expectations are met and non-0 otherwise.

   The accompanying program b.c reverts the character order.  When run as

     a b

   the progrm will fail, since piped stdin/stdout will become fully-buffered.

   When run as

     a ion b

   the program will succeed, since "ion" forces line-buffering of the program
   streams.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>

char prompt[] = "rev> ";
int promptlen = sizeof(prompt) - 1;
char *input[] = { "adam", "word", "revert", NULL };
char *output[] = { "mada", "drow", "trever", NULL };

int
main(int argc, char **argv)
{
	int ip[2], op[2];
	pid_t pid;
	int i;
	FILE *ifile, *ofile;
	char buf[128];
	
	assert(argc > 1);
	argc--;
	argv++;
	assert(pipe(ip) == 0);
	assert(pipe(op) == 0);

	pid = fork();
	assert(pid != -1);

	if (pid == 0) {
		/* ip[0] - child stdin
		   ip[1] - master write
		   op[0] - master read
		   op[1] - child stdout
		*/
		close(ip[1]);
		close(op[0]);
		if (ip[0] != 0) {
			close(0);
			dup(ip[0]);
			close(ip[0]);
		}
		if (op[1] != 1) {
			close(1);
			dup(op[1]);
			close(op[1]);
		}
		execvp(argv[0], argv);
		perror(argv[0]);
		_exit(127);
	}
	/* master */
	close(ip[0]);
	close(op[1]);
	ifile = fdopen(op[0], "r");
	if (!ifile) {
		perror("op[0]");
		abort();
	}
	setlinebuf(ifile);
	ofile = fdopen(ip[1], "w");
	if (!ofile) {
		perror("ip[1]");
		abort();
	}
	setlinebuf(ofile);
	for (i = 0; input[i]; i++) {
		alarm(5);
		if (fread(buf, promptlen, 1, ifile) != 1) {
			fprintf(stderr, "%d: read error\n", __LINE__);
			abort();
		}
		alarm(0);

		if (strncmp(buf, prompt, promptlen)) {
			fprintf(stderr, "%d: expected prompt, but got \"%*.*s\"\n",
				__LINE__, promptlen, promptlen, buf);
			abort();
		}

		fprintf(ofile, "%s\n", input[i]);

		alarm(5);
		if (!fgets(buf, sizeof(buf), ifile)) {
			fprintf(stderr, "%d: read error\n", __LINE__);
			abort();
		}
		alarm(0);
		buf[strlen(buf)-1] = 0;
		if (strcmp(buf, output[i])) {
			fprintf(stderr, "%d: expected \"%s\", but got \"%s\"\n",
				__LINE__, output[i], buf);
			abort();
		}
	}
	return 0;
}
		
