/* A simple rev(1)-like program for testing ion.
   Copyright (C) 2013-2024 Sergey Poznyakoff

   Ion is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Ion is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with ion. If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <string.h>

int
main()
{
	int i, j;
	char buf[128];
	static char prompt[] = "rev> ";

	while (1) {
		fwrite(prompt, strlen(prompt), 1, stdout);
		if (!fgets(buf, sizeof(buf), stdin))
			break;
		j = strlen(buf) - 1;
		if (buf[j] == '\n')
			buf[j--] = 0;
		for (i = 0; i < j; i++, j--) {
			int c = buf[j];
			buf[j] = buf[i];
			buf[i] = c;
		}
		fprintf(stdout, "%s\n", buf);
	}
	return 0;
}
