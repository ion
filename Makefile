# This file is part of ion.
# Copyright (C) 2013-2024 Sergey Poznyakoff
#
# Ion is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3 of the License, or (at your
# option) any later version.
#
# Ion is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with ion. If not, see <http://www.gnu.org/licenses/>.

# Compiler options
CFLAGS = -ggdb -Wall

# Installation prefix
PREFIX = /usr/local
BINDIR = $(PREFIX)/bin
MANDIR = $(PREFIX)/share/man

MAN1DIR = $(MANDIR)/man1

# Install program.  Use cp(1) if not available.
INSTALL = install
# Program to make directory hierarchy.
MKHIER  = install -d

## ################################################################## ## 
## There is nothing to modify below this line                         ##

PACKAGE = ion
VERSION = 1.0

ion: ion.c
	cc $(CFLAGS) -oion -DCANONICAL_NAME=\"$(PACKAGE)\" ion.c
check: 
	make -C test check
clean: 
	rm -f ion *.o test/*.o test/*.out

# ###################
# Installation rules
# ###################
install: install-bin install-man

install-bin: ion
	$(MKHIER) $(DESTDIR)$(BINDIR)
	$(INSTALL) ion $(DESTDIR)$(BINDIR)

install-man:
	$(MKHIER) $(DESTDIR)$(MAN1DIR)
	$(INSTALL) ion.1 $(DESTDIR)$(MAN1DIR)

# ###################
# Distribution rules
# ###################
DOCS = COPYING README NEWS
DISTDIR   = $(PACKAGE)-$(VERSION)
DISTFILES = ion.c ion.1 Makefile test/Makefile test/a.c test/b.c $(DOCS)

distdir:
	rm -rf $(DISTDIR) && \
	mkdir $(DISTDIR) &&  \
	tar cf - $(DISTFILES) | tar -C $(DISTDIR) -xf -

dist: distdir
	tar cfz $(DISTDIR).tar.gz $(DISTDIR)
	rm -rf $(DISTDIR)

distcheck: dist
	tar xfz $(DISTDIR).tar.gz
	@if $(MAKE) -C $(DISTDIR) $(DISTCHECKFLAGS) && \
            $(MAKE) -C $(DISTDIR) check && \
	    $(MAKE) -C $(DISTDIR) install DESTDIR=_inst; then \
	    echo "$(DISTDIR).tar.gz ready for distribution"; \
	    rm -rf $(DISTDIR); \
	else \
	    exit 2; \
	fi



