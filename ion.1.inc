.if "\*[.T]"html" \{\
.SH DOWNLOADS
.PP
.B Ion
is available in source form from its
.URL http://git.gnu.org.ua/cgit/ion.git "git repository" .
.PP
Recent news, changes and bugfixes can be tracked from the
.URL http://puszcza.gnu.org.ua/projects/ion "project's development page" .

\}

