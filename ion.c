/* ion - run a program with its I/O streams line-buffered.
   Copyright (C) 2013-2024 Sergey Poznyakoff

   Ion is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Ion is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with ion. If not, see <http://www.gnu.org/licenses/>. */

#define _XOPEN_SOURCE 600
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <termios.h>

#ifndef TCSASOFT
# define TCSASOFT 0
#endif

#define EX_OK 0
#define EX_USAGE 125
#define EX_ERR   126
#define EX_EXEC  127

struct buffer {
	char buf[BUFSIZ];
	int avail;
	int written;
	int cr;
};

#define shut(fildes) do {					\
	close(fildes);						\
	fildes = -1;						\
} while(0)
#define bufinit(buf) do { (buf).avail = (buf).written = (buf).cr = 0; } while(0)
#define bufisempty(buf) ((buf).avail == (buf).written)
#define bufread(buffer,fildes) do {				\
	int r = read(fildes, (buffer).buf + (buffer).avail,	\
		     sizeof((buffer).buf) - (buffer).avail);	\
	if (r < 1)						\
		shut(fildes);					\
	else							\
		(buffer).avail += r;				\
} while(0);
#define bufwrite(buffer,fildes) do {				\
	int r = write(fildes, (buffer).buf + (buffer).written,	\
		      (buffer).avail - (buffer).written);	\
	if (r < 1)						\
		shut(fildes);					\
	else							\
		(buffer).written += r;				\
} while(0);

void
tr(struct buffer *bp)
{
	int i,j;

	for (i = j = bp->written; i < bp->avail; ) {
		if (bp->buf[i] == '\r') {
			bp->cr = 1;
			i++;
		} else {
			if (bp->cr) {
				bp->cr = 0;
				if (bp->buf[i] != '\n')
					bp->buf[j++] = '\r';
			}
			bp->buf[j++] = bp->buf[i++];
		}
	}
	bp->avail = j;
}

int stop;
int status;

void
sigchld(int sig)
{
	wait(&status);
	stop = 1;
}

void
noecho(int fd)
{
	struct termios to;

	if (tcgetattr(fd, &to)) {
		perror("tcgetattr");
		exit(EX_ERR);
	}
	to.c_lflag &= ~(ECHO | ISIG);
	if (tcsetattr(fd, TCSAFLUSH | TCSASOFT, &to)) {
		perror("tcgetattr");
		exit(EX_ERR);
	}
}

static int
match_canonical_name(char **argv)
{
#ifdef CANONICAL_NAME
	char *p, *pn, *path, *newpath;
	dev_t dev;
	ino_t ino;
	struct stat st;
	char *bufptr = NULL;
	size_t bufsize = 0;
	int found;

	pn = strrchr(argv[0], '/');
	if (pn) {
		if (stat(argv[0], &st)) {
			perror("stat");
			return 1;
		}
		dev = st.st_dev;
		ino = st.st_ino;

		*pn++ = 0;
	} else {
		pn = argv[0];
		dev = 0;
		ino = 0;
	}

	if (strcmp(pn, CANONICAL_NAME) == 0)
		return 1;

	path = getenv("PATH");
	if (!path)
		return 1;
	newpath = malloc(strlen(path) + 2);
	if (!newpath)
		abort();
	newpath[0] = 0;
	found = 0;
	for (p = strtok(path, ":"); p; p = strtok(NULL, ":")) {
		if (found != 2) {
			int len = strlen(p), ns;
			while (len > 0 && p[len-1] == '/')
				len--;
			ns = len + 1 + strlen(pn) + 1;

			if (!bufptr) {
				bufsize = ns;
				bufptr = malloc(bufsize);
			} else if (ns > bufsize) {
				bufsize = ns;
				bufptr = realloc(bufptr, bufsize);
			}
			if (!bufptr) {
				perror("memory allocation");
				abort();
			}
			memcpy(bufptr, p, len);
			bufptr[len++] = '/';
			strcpy(bufptr + len, pn);
			if (access(bufptr, X_OK) == 0) {
				if (ino) {
					if (stat(bufptr, &st)) {
						/* perror(bufptr); */
					} else {
						if (!(st.st_ino == ino &&
						      st.st_dev == dev)) {
							argv[0] = bufptr;
							found = 2;
						}
						continue;
					}
				} else if (++found == 1)
					continue;
				else
					argv[0] = bufptr;
			}
		}
		strcat(newpath, ":");
		strcat(newpath, p);
	}

	if (found == 2) {
		setenv("PATH", newpath + 1, 1);
		free(newpath);
		return 0;
	}

	free(newpath);
#endif
	return 1;
}

char *usage[] = {
	"usage: ion PROGRAM [ARGS...]",
	"",
	"Ion runs PROGRAM with PTY attached to its stdin, stdout and stderr.",
	"Use it to ensure the PROGRAM's I/O streams are line-buffered.",
	"",
#ifdef CANONICAL_NAME
	"Unless invoked as \"" CANONICAL_NAME
	"\", this utility scans the PATH for an",
	"executable with the same name and invokes it.  When using this",
	"invocation method, make sure \"ion\" (or a symlink to it) precedes",
	"the actual executable in the PATH.",
	"",
#endif
	"Report bugs and suggestions to <gray@gnu.org>.",
	NULL
};

int
main(int argc, char **argv)
{
	int i;
	int master, slave;
	char *sn;
	pid_t pid;
	fd_set rdset;
	struct buffer ibuf, obuf;
	int in = 0, out = 1;

	if (match_canonical_name(argv)) {
		argc--;
		argv++;

		if (argc == 0) {
			for (i = 0; usage[i]; i++) {
				fputs(usage[i], stderr);
				fputc('\n', stderr);
			}
			return EX_USAGE;
		}
	}

	master = posix_openpt(O_RDWR);
	if (master == -1) {
		perror("posix_openpty");
		return EX_ERR;
	}

	if (grantpt(master)) {
		perror("grantpt");
		return EX_ERR;
	}

	if (unlockpt(master)) {
		perror("unlockpt");
		return EX_ERR;
	}

	sn = ptsname(master);

/*	printf("opening pty %s\n", sn);*/

	slave = open(sn, O_RDWR);
	if (slave < 0) {
		perror("open");
		return EX_ERR;
	}

	signal(SIGCHLD, sigchld);

	pid = fork();
	if (pid == -1) {
		perror("fork");
		return EX_ERR;
	}

	if (pid == 0) {
		noecho(slave);
		for (i = 0; i < 3; i++) {
			if (slave != i) {
				close(i);
				if (dup(slave) != i) {
					perror("dup");
					_exit(EX_EXEC);
				}
			}
		}
		for (; i < sysconf(_SC_OPEN_MAX); i++)
			close(i);
		execvp(argv[0], argv);
		perror(argv[0]);
		_exit(EX_EXEC);
	}

	bufinit(ibuf);
	bufinit(obuf);
	for (stop = 0; !stop;) {
		FD_ZERO(&rdset);
		if (in != -1) FD_SET(in, &rdset);
		if (master != -1) FD_SET(master, &rdset);

		if (select(master + 1, &rdset, NULL, NULL, NULL) < 0) {
			if (errno == EINTR) {
				if (stop)
					shut(master);
				continue;
			}
			perror("select");
			exit(EX_ERR);
		}

		if (in >= 0 && FD_ISSET(in, &rdset)) bufread(ibuf, in);

		if (master >= 0 && FD_ISSET(master, &rdset))
			bufread(obuf, master);

		if (out >= 1 && !bufisempty(obuf)) {
			tr(&obuf);
			bufwrite(obuf, out);
		}

		if (master > 0 && !bufisempty(ibuf)) bufwrite(ibuf, master);

		if (bufisempty(ibuf)) bufinit(ibuf);
		if (bufisempty(obuf)) {
			bufinit(obuf);
			if (master < 0) stop = 1;
		}
	}

	if (WIFEXITED(status))
		return WEXITSTATUS(status);

	if (WIFSIGNALED(status))
		fprintf(stderr, "ion: child process %s failed on signal %d\n",
			argv[0], WTERMSIG(status));
	else
		fprintf(stderr, "ion: child process %s failed\n", argv[0]);
	return EX_EXEC;
}
